package net.jevring.timex;

import net.jevring.timex.calendar.DateRange;
import net.jevring.timex.calendar.DateRangeView;
import net.jevring.timex.resizeable.ComponentEditor;
import net.jevring.timex.resizeable.Resizable;

import javax.swing.*;
import java.awt.*;
import java.util.Calendar;

/**
 * @author markus@jevring.net
 *         Time: 2011-08-13 20:08
 */
public class Timex extends JPanel {
    public Timex() {
        setPreferredSize(new Dimension(1024, 768));
        setLayout(new BorderLayout());
        Calendar from = Calendar.getInstance();
        Calendar to = Calendar.getInstance();
        to.add(Calendar.DAY_OF_YEAR, 7);
        add(new DateRangeView(new DateRange(from, to)), BorderLayout.CENTER);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                JFrame frame = new JFrame("Timex");
                frame.setContentPane(new Timex());
                frame.pack();
                frame.setVisible(true);
                Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
                int x = (int) ((screenSize.getWidth() / 2) - (frame.getSize().getWidth() / 2));
                int y = (int) ((screenSize.getHeight() / 2) - (frame.getSize().getHeight() / 2));
                frame.setLocation(x, y);
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            }
        });
    }
}
