package net.jevring.timex.calendar;

import net.jevring.timex.resizeable.TransformationListener;
import net.jevring.timex.resizeable.Resizable;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Calendar;
import java.util.Locale;

/**
 * @author markus@jevring.net
 */
public class DateRangeView extends JPanel {

    public DateRangeView(DateRange dateRange) {
        // todo: gray weekends and outside working hours, if option enabled
        setLayout(new BorderLayout());
        final JTable calendar = new JTable(new DateRangeTableModel(dateRange));
        calendar.setCellSelectionEnabled(true);
        calendar.getTableHeader().setReorderingAllowed(false);


        JScrollPane scrollPane = new JScrollPane(calendar);
        // _todo: when scrolling horizontally, only chunk whole days
        // Actually, should we be able to "scroll" like that?
        // I think we should have views that are N days large, and if we want to see another view,
        // we change the date range (or create a new with a different date range.
        // otherwise we have to float the actual calendar inside the time stamps and stuff.
        add(scrollPane, BorderLayout.NORTH);
        // move it to whatever the start of the day is (16 == 8, etc)
        calendar.scrollRectToVisible(calendar.getCellRect(16, 0, true));

        calendar.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                // todo: should we pop up the issue selector first or let people set it later?
                // perhaps it can be of a different color until the issue is chosen?
                // also, we should be able to change the issue.
                // this should be a setting, whether to pop it open directly or not.
                // auto-complete with typing, of course
                calendar.clearSelection();
                final Resizable r = new Resizable(null);
                calendar.add(r); // if we do this after we do .setBounds(), the whole thing will not be visible.
                Rectangle bounds = calendar.getCellRect(calendar.rowAtPoint(e.getPoint()), calendar.columnAtPoint(e.getPoint()), true);
                bounds.setSize(bounds.width, bounds.height * 4);
                r.setBounds(bounds);
                r.addMovementListener(new TransformationListener() {
                    private MovementTarget movementTarget;
                    public void componentTransforming(Component component) {
                        // highlight landing target
                        MovementTarget movementTarget = getMovementTarget(component, calendar);
                        // todo: this must still update the rows, even if the column is wrong
                        this.movementTarget = movementTarget;
                        calendar.setRowSelectionInterval(movementTarget.getStartingRow(), movementTarget.getEndingRow());
                        calendar.setColumnSelectionInterval(movementTarget.getColumn(), movementTarget.getColumn());
                    }

                    public void componentTransformed(Component component) {
                        // move it to the actual highlighted target
                        if (movementTarget != null) {
                            Rectangle top = calendar.getCellRect(movementTarget.getStartingRow(), movementTarget.getColumn(), true);
                            Rectangle bottom = calendar.getCellRect(movementTarget.getEndingRow(), movementTarget.getColumn(), true);
                            component.setBounds(top.union(bottom));

                            // todo: change the whole description here: ticket + time range + length + comment
                            r.setDescription("(" + movementTarget.getStartingRow() + ", " + movementTarget.getColumn() + ")");
                            r.setDescription("<html>Rectangle top = calendar.getCellRect(movementTarget.getStartingRow(), movementTarget.getColumn(), true);<br>" + "                            Rectangle bottom = calendar.getCellRect(movementTarget.getEndingRow(), movementTarget.getColumn(), true);<br>" + "                            component.setBounds(top.union(bottom));");
                            calendar.clearSelection();
                        }
                        // todo: this snaps to one more row than it should, fix this.
                    }
                });
            }
        });
    }

    private MovementTarget getMovementTarget(Component component, JTable calendar) {
        Rectangle loc = component.getBounds();

        Point topRightCorner = new Point(loc.x + loc.width, loc.y);
        Point topLeftCorner = loc.getLocation();
        Point bottomRightCorner = new Point(loc.x + loc.width, loc.y + loc.height);
        Point bottomLeftCorner = new Point(loc.x, loc.y + loc.height);

        Rectangle rightTargetCell = calendar.getCellRect(calendar.rowAtPoint(topRightCorner), calendar.columnAtPoint(topRightCorner), true);
        int distanceInRightTargetCell = topRightCorner.x - rightTargetCell.x;

        int startingRow;
        int endingRow;
        int col;
        if (distanceInRightTargetCell > rightTargetCell.width / 2) {
            startingRow = calendar.rowAtPoint(topRightCorner);
            endingRow = calendar.rowAtPoint(bottomRightCorner);
            col = calendar.columnAtPoint(topRightCorner);
        } else {
            startingRow = calendar.rowAtPoint(topLeftCorner);
            endingRow = calendar.rowAtPoint(bottomLeftCorner);
            col = calendar.columnAtPoint(topLeftCorner);
        }

        // always start at column 1, since 0 is the time
        return new MovementTarget(startingRow, endingRow, Math.max(1, col));
    }

    private class DateRangeTableModel extends AbstractTableModel {
        private final DateRange dateRange;

        public DateRangeTableModel(DateRange dateRange) {
            this.dateRange = dateRange;
        }

        @Override
        public String getColumnName(int column) {
            if (column == 0) {
                // first column is for time only
                return null;
            }
            Calendar day = Calendar.getInstance();
            day.setTimeInMillis(dateRange.getFrom().getTimeInMillis());
            day.add(Calendar.DAY_OF_YEAR, column - 1); // -1 because of the first time column
            // todo: check for year rollover etc.
            // todo: should we do local locale?
            String weekday = day.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.ENGLISH);
            return weekday + " " + day.get(Calendar.DAY_OF_MONTH) + " / " + day.get(Calendar.MONTH);
        }

        @Override
        public int getRowCount() {
            return 48;
        }

        @Override
        public int getColumnCount() {
            return dateRange.days() + 1;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            if (columnIndex == 0) {
                // todo: formatting should make this whole thing look nice, like no lines for half hours in the first column, etc.
                return getTime(rowIndex);
            } else {
                return null;
            }
        }

        private String getTime(int rowIndex) {
            if (rowIndex % 2 == 0) {
                int hour = rowIndex / 2;
                // todo: time formatting...
                return hour + ":00";
            } else {
                return null;
            }
        }
    }
}
