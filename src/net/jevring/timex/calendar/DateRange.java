package net.jevring.timex.calendar;

import java.util.Calendar;

/**
 * @author markus@jevring.net
 */
public class DateRange {
    private final Calendar from;
    private final Calendar to;

    public DateRange(Calendar from, Calendar to) {
        this.from = from;
        this.to = to;
        if (from.after(to)) {
            throw new IllegalArgumentException("From date must be before to date");
        }
    }

    public Calendar getFrom() {
        return from;
    }

    public Calendar getTo() {
        return to;
    }

    public int days() {
        if (from.get(Calendar.YEAR) == to.get(Calendar.YEAR)) {
            return to.get(Calendar.DAY_OF_YEAR) - from.get(Calendar.DAY_OF_YEAR);
        } else {
            // todo: do overlap year counting and check for leapyears and stuff
            return 1;
        }
    }
}
