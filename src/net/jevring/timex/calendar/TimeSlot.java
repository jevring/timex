package net.jevring.timex.calendar;

import java.util.Calendar;

/**
 * @author markus@jevring.net
 */
public class TimeSlot {
    private final Calendar day;
    private final Time from;
    private final Time to;

    public TimeSlot(Calendar day, Time from, Time to) {
        this.from = from;
        this.to = to;
        this.day = day;
    }
}
