package net.jevring.timex.calendar;

/**
 * @author markus@jevring.net
 */
public class MovementTarget {
    private final int startingRow;
    private final int endingRow;
    private final int column;

    public MovementTarget(int startingRow, int endingRow, int column) {
        this.startingRow = startingRow;
        this.endingRow = endingRow;
        this.column = column;
    }

    public int getStartingRow() {
        return startingRow;
    }

    public int getEndingRow() {
        return endingRow;
    }

    public int getColumn() {
        return column;
    }
}
