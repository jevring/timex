package net.jevring.timex;

import net.jevring.timex.calendar.TimeSlot;

/**
 * @author markus@jevring.net
 */
public class Work {
    private String issue;
    private TimeSlot timeSlot;
    private String comment;

    public Work(String issue, TimeSlot timeSlot, String comment) {
        this.issue = issue;
        this.timeSlot = timeSlot;
        this.comment = comment;
    }
}
