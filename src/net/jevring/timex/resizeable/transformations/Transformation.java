package net.jevring.timex.resizeable.transformations;

import java.awt.*;

/**
 * @author markus@jevring.net
 */
public interface Transformation {
    public Rectangle getBounds(int deltax, int deltay, Point dragStartLocation, Dimension dragStartSize);
}
