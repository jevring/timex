package net.jevring.timex.resizeable.transformations;

import java.awt.*;

/**
 * @author markus@jevring.net
 */
public class Transformations {
    private Transformations() {}

    public static final Transformation N = new Transformation() {
        @Override
        public Rectangle getBounds(int deltax, int deltay, Point dragStartLocation, Dimension dragStartSize) {
            return new Rectangle(dragStartLocation.x, dragStartLocation.y + deltay, dragStartSize.width, dragStartSize.height - deltay);
        }
    };

    public static final Transformation S = new Transformation() {
        @Override
        public Rectangle getBounds(int deltax, int deltay, Point dragStartLocation, Dimension dragStartSize) {
            return new Rectangle(dragStartLocation.x, dragStartLocation.y, dragStartSize.width, dragStartSize.height + deltay);
        }
    };

    public static final Transformation E = new Transformation() {
        @Override
        public Rectangle getBounds(int deltax, int deltay, Point dragStartLocation, Dimension dragStartSize) {
            return new Rectangle(dragStartLocation, new Dimension(dragStartSize.width + deltax, dragStartSize.height));
        }
    };

    public static final Transformation W = new Transformation() {
        @Override
        public Rectangle getBounds(int deltax, int deltay, Point dragStartLocation, Dimension dragStartSize) {
            return new Rectangle(dragStartLocation.x + deltax, dragStartLocation.y, dragStartSize.width - deltax, dragStartSize.height);
        }
    };

    public static final Transformation NE = new Transformation() {
        @Override
        public Rectangle getBounds(int deltax, int deltay, Point dragStartLocation, Dimension dragStartSize) {
            return new Rectangle(dragStartLocation.x, dragStartLocation.y + deltay, dragStartSize.width + deltax, dragStartSize.height - deltay);
        }
    };

    public static final Transformation NW = new Transformation() {
        @Override
        public Rectangle getBounds(int deltax, int deltay, Point dragStartLocation, Dimension dragStartSize) {
            return new Rectangle(dragStartLocation.x + deltax, dragStartLocation.y + deltay, dragStartSize.width - deltax, dragStartSize.height - deltay);
        }
    };

    public static final Transformation SE = new Transformation() {
        @Override
        public Rectangle getBounds(int deltax, int deltay, Point dragStartLocation, Dimension dragStartSize) {
            return new Rectangle(dragStartLocation.x, dragStartLocation.y, dragStartSize.width + deltax, dragStartSize.height + deltay);
        }
    };

    public static final Transformation SW = new Transformation() {
        @Override
        public Rectangle getBounds(int deltax, int deltay, Point dragStartLocation, Dimension dragStartSize) {
            return new Rectangle(dragStartLocation.x + deltax, dragStartLocation.y, dragStartSize.width - deltax, dragStartSize.height + deltay);
        }
    };

    public static final Transformation MOVE = new Transformation() {
        @Override
        public Rectangle getBounds(int deltax, int deltay, Point dragStartLocation, Dimension dragStartSize) {
            return new Rectangle(dragStartLocation.x + deltax, dragStartLocation.y + deltay, dragStartSize.width, dragStartSize.height);
        }
    };
    
    public static Transformation combine(Transformation h, Transformation v) {
        if (h == null && v == null) {
            return MOVE;
        } else if (h == null) {
            return v;
        } else if (v == null) {
            return h;
        } else {
            if (v == N && h == E) {
                return NE;
            } else if (v == N && h == W) {
                return NW;
            } else if (v == S && h == E) {
                return SE;
            } else if (v == S && h == W) {
                return SW;
            }
        }
        throw new IllegalArgumentException("Transformations '" + h + "' and '" + v + "' are not a valid combination");

    }


}
