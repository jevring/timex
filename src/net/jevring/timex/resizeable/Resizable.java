package net.jevring.timex.resizeable;

import net.jevring.timex.resizeable.transformations.Transformation;
import net.jevring.timex.resizeable.transformations.Transformations;

import javax.sound.midi.VoiceStatus;
import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;

/**
 * @author markus@jevring.net
 *         Time: 2011-08-13 20:16
 */
public class Resizable extends JPanel {
    private final List<TransformationListener> transformationListeners = new LinkedList<TransformationListener>();
    private final ComponentEditor componentEditor;
    private Point dragStartPoint = null;
    private Dimension dragStartSize = null;
    private Point dragStartLocation = null;
    private Transformation transformation = null;


    private static final Map<Transformation, Cursor> cursors = new HashMap<Transformation, Cursor>();
    private JLabel description;

    static {
        cursors.put(Transformations.N, Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR));
        cursors.put(Transformations.S, Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR));
        cursors.put(Transformations.E, Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR));
        cursors.put(Transformations.W, Cursor.getPredefinedCursor(Cursor.W_RESIZE_CURSOR));
        cursors.put(Transformations.NE, Cursor.getPredefinedCursor(Cursor.NE_RESIZE_CURSOR));
        cursors.put(Transformations.NW, Cursor.getPredefinedCursor(Cursor.NW_RESIZE_CURSOR));
        cursors.put(Transformations.SE, Cursor.getPredefinedCursor(Cursor.SE_RESIZE_CURSOR));
        cursors.put(Transformations.SW, Cursor.getPredefinedCursor(Cursor.SW_RESIZE_CURSOR));
        cursors.put(Transformations.MOVE, null); // alternatively this could be the movement cursor
    }

    /*
    Add the resizable to something where it can move and resize in snaps
    Enable us to alter some property of the resizable.
    - Perhaps not containing arbitrary components, but having a "title bar" or double-clicking to edit some options.
    Mouse-over should show context details, (like which ticket it is, and how long the time interval is, and from when and to when it spans)

    What's stopping someone from just using outlook, which already has all this functionality, to do this?
    Well, it only has the calendaring gui. The integration with JIRA is the kicker

     */

    public Resizable(final ComponentEditor componentEditor) {
        this.componentEditor = componentEditor;
        setLayout(new BorderLayout());
        // todo: why doesn't the label show up until we create a new resizable?
        description = new JLabel();
        description.setHorizontalAlignment(JLabel.CENTER);
        add(description, BorderLayout.NORTH);
        setOpaque(false);

        setDoubleBuffered(true);
        setBorder(BorderFactory.createLineBorder(Color.RED, 10));

        // NOTE: it seems that we don't need to forward the events.
        // at least not with JLabel. Perhaps with the rest though.
        // try without until we run in to trouble.
        add(forwardEvents(new JButton("test")), BorderLayout.SOUTH);

        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                setResizeCursor(e);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                setCursor(null);
            }

            @Override
            public void mousePressed(MouseEvent e) {
                // NOTE: This HAS to be "LocationOnScreen", since otherwise it will be relative to the
                // moved component when we move, which means we flicker.
                dragStartPoint = e.getLocationOnScreen();
                dragStartSize = getSize();
                dragStartLocation = getLocation();
                transformation = getTransformation(e);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                updateSizeOrLocation(e, false);
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                if (componentEditor != null && e.getClickCount() == 2) {
                    componentEditor.onEdit(Resizable.this);
                }
            }
        });
        addMouseMotionListener(new MouseMotionListener() {
            public void mouseDragged(MouseEvent e) {
                updateSizeOrLocation(e, true);
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                setResizeCursor(e);
            }
        });
    }

    private JComponent forwardEvents(JComponent component) {
        component.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Resizable.this.dispatchEvent(e);
            }

            @Override
            public void mousePressed(MouseEvent e) {
                Resizable.this.dispatchEvent(e);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                Resizable.this.dispatchEvent(e);
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                Resizable.this.dispatchEvent(e);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                Resizable.this.dispatchEvent(e);
            }
        });
        component.addMouseMotionListener(new MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {
                Resizable.this.dispatchEvent(e);
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                Resizable.this.dispatchEvent(e);
            }
        });
        return component;
    }

    public void setDescription(String description) {
        this.description.setText(description);
    }

    private void setResizeCursor(MouseEvent e) {
        Transformation transformation = getTransformation(e);
        if (transformation != null) {
            setCursor(cursors.get(transformation));
        } else {
            setCursor(null);
        }
    }

    private void updateSizeOrLocation(MouseEvent e, boolean stillMoving) {
        if (dragStartPoint != null && dragStartSize != null && dragStartLocation != null) {
            // NOTE: This HAS to be "LocationOnScreen", since otherwise it will be relative to the
            // moved component when we move, which means we flicker.

            // find out in which direction and how far we are being dragged
            int dx = e.getXOnScreen() - dragStartPoint.x;
            int dy = e.getYOnScreen() - dragStartPoint.y;

            setBounds(transformation.getBounds(dx, dy, dragStartLocation, dragStartSize));
            // doing both here doesn't work.
            // the only thing that works is doing repaint() only
            //revalidate();
            repaint();

            for (TransformationListener transformationListener : transformationListeners) {
                if (stillMoving) {
                    transformationListener.componentTransforming(this);
                } else {
                    transformationListener.componentTransformed(this);
                }
            }
        }
        // todo: redraw only the changed part, as per:
        // http://download.oracle.com/javase/tutorial/uiswing/events/mousemotionlistener.html
        // look for ".union("
    }

    /**
     * Determines which side, or corner, the event was on. If it is not inside the boundaries
     * of the border, this method will return null.
     *
     * @param e the mouse event.
     * @return The side or {@code null} if not on the borders.
     */
    private Transformation getTransformation(MouseEvent e) {
        Border border = getBorder();
        Insets borderInsets = border.getBorderInsets(this);
        Point p = e.getPoint();
        int w = getWidth();
        int h = getHeight();
        Transformation horizontalSide = null;
        Transformation verticalSide = null;
        if (p.x > w - borderInsets.right) {
            // Resizing the right side
            horizontalSide = Transformations.E;
        } else if (p.x < borderInsets.left) {
            // Resizing the left side
            horizontalSide = Transformations.W;
        }

        if (p.y > h - borderInsets.bottom) {
            // Resizing the bottom
            verticalSide = Transformations.S;
        } else if (p.y < borderInsets.top) {
            // Resizing the top
            verticalSide = Transformations.N;
        }

        return Transformations.combine(horizontalSide, verticalSide);
    }

    public void addMovementListener(TransformationListener transformationListener) {
        // todo: javadoc that this isn't thread safe
        transformationListeners.add(transformationListener);
    }
}
