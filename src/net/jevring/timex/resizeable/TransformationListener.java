package net.jevring.timex.resizeable;

import java.awt.*;

/**
 * @author markus@jevring.net
 */
public interface TransformationListener {
    public void componentTransforming(Component component);
    public void componentTransformed(Component component);
}
