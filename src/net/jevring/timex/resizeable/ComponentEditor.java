package net.jevring.timex.resizeable;

import java.awt.*;

/**
 * @author markus@jevring.net
 */
public interface ComponentEditor {
    public void onEdit(Component component); // todo: might pass more than this
}
